---
title: Blogging is Hard
date: '2017-03-17T00:00:00.000Z'
description: ''
tags:
  - Blogging
  - Life Lessons
---
I just felt like sharing this thought that blogging is hard. You really need a thick skin (which I lack). I shared my last post to reddit and then felt discouraged by the negativity. Of the hundred or so reads over the past day, 4% wrote negative comments and 96% people didn’t say anything. The comments were dumb. By dumb I mean non-constructive, and showing some lack of understanding. In their defense, a friend pointed out that I didn’t really offer much detailed explanation, so probably some people didn’t understand what I was getting at. Fair enough. Can’t please everybody.

The same friend offered this advice which I appreciated:

> **Friend**: Don’t let those asshats get you down. You know why?\
> **Me**: Why?\
> **Friend**: It’s the same thing I tell people if they start bashing a presenter at a conference or something. (S)he’s​ at least up their speaking. You’re putting forth the effort and putting yourself out there. They’re likely wasting their days stuck on reddit. It’s hard to put yourself out there. :( And it is easy to write comments and criticize other’s efforts.

For the individuals putting themselves out there, I offer my empathy and support. Keep at it. Chin up!
