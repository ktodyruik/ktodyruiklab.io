---
title: Web Server One-Liners
date: '2016-01-21T00:00:00.000Z'
description: ''
tags:
  - JavaScript 
  - Programming
  - Web Development 
---
While doing some HTML/Javascript development I came across this [Big List of HTTP Static Server One Liners](https://gist.github.com/willurd/5720255).

For example, to quickly stand up a web server using Node.js, first install http-server globally with the Node Package Manager:

```
$ npm install -g http-server
```

Then, whenever you need a quick web server, change directory to where your html files are located, and run this command:

```
$ http-server -p 8000
```

Open a browser and point it to http://localhost:8000 to view your pages.


